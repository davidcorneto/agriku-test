import 'package:agriku/src/blocs/bloc.dart';
import 'package:agriku/src/screens/home_screen.dart';
import 'package:agriku/src/utilities/utilities.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';

class SplashBloc implements Bloc{
  BuildContext context;

  SplashBloc(){
    splashDelay();
  }

  splashDelay() async{
    await Utilities.addDelay(3);
    goToHome(context);
  }

  @override
  void dispose() {
    // TODO: implement dispose
  }

  goToHome(BuildContext context){
    Navigator.popAndPushNamed(context, HomeScreen.routeName);
  }
}
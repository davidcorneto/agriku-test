import 'package:agriku/src/blocs/bloc.dart';
import 'package:agriku/src/models/dota_hero_list_response_model.dart';
import 'package:agriku/src/resources/api_response.dart';
import 'package:agriku/src/resources/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:rxdart/rxdart.dart';

class HomeHeroListBloc implements Bloc{
  @override
  BuildContext context;
  Repository _repository = Repository();

  final List<String> categoryHero = <String>['A', 'B', 'C', 'D'];

  final _listHero = BehaviorSubject<ApiResponse<List<DotaHeroListResponseModel>>>();

  Stream<ApiResponse<List<DotaHeroListResponseModel>>> get listHero =>
      _listHero.stream;

  Function(ApiResponse<List<DotaHeroListResponseModel>>) get addListHero =>
      _listHero.sink.add;

  ApiResponse<List<DotaHeroListResponseModel>> get listHeroValue =>
      _listHero.value;

  HomeHeroListBloc(){
    getData();
  }

  getData() async{
    addListHero(ApiResponse.loading());
    try {
      final response = await _repository.getListHero();
      addListHero(ApiResponse.completed(response));
    } catch (e) {
      addListHero(ApiResponse.error(e.toString()));
    }
  }

  itemClicked(String productId) {
    // Navigator.pushNamed(context, BuyDetailScreen.routeName,
    //     arguments: BuyDetailArgument(
    //       productId: productId,
    //       destinationHeroTag: getHeroTag(productId),
    //     ));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _listHero.close();
  }
}
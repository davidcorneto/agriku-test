import 'package:agriku/src/blocs/bloc.dart';
import 'package:agriku/src/blocs/bloc_provider.dart';
import 'package:agriku/src/blocs/home/home_hero_list_bloc.dart';
import 'package:agriku/src/screens/home_hero_list_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';

class HomeBloc implements Bloc{
  BuildContext context;


  List<Widget> tabBarViewChildren;

  List<Widget> getTabBarViewChildren() {
    if (tabBarViewChildren == null) {
      tabBarViewChildren = [
        BlocProvider<HomeHeroListBloc>(
          bloc: HomeHeroListBloc(),
          child: HomeHeroListScreen(),
        ),
        BlocProvider<HomeHeroListBloc>(
          bloc: HomeHeroListBloc(),
          child: HomeHeroListScreen(),
        ),
      ];
    }
    return tabBarViewChildren;
  }
  @override
  void dispose() {
    // TODO: implement dispose
  }
}
import 'dart:async';

class Validators {
  final validateEmail =
  StreamTransformer<String, String>.fromHandlers(handleData: (email, sink) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    if (email.isEmpty) {
      sink.addError('* Email harus diisi');
    } else if (!regex.hasMatch(email)) {
      sink.addError('* Format email tidak sesuai');
    } else {
      sink.add(email);
    }
  });

  final validatePassword = StreamTransformer<String, String>.fromHandlers(
      handleData: (password, sink) {
        if (password.isEmpty) {
          sink.addError('* Password harus diisi');
        } else if (password.length < 6) {
          sink.addError('* Password harus paling sedikit 6 karakter');
        } else {
          sink.add(password);
        }
      });
}
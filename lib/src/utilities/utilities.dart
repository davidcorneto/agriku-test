import 'package:agriku/src/constants/agriku_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Utilities {
  static final GlobalKey<NavigatorState> navigatorKey =
  new GlobalKey<NavigatorState>();

  static void dismissKeyboard(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  static Future<void> addDelay(int seconds) async {
    await Future.delayed(Duration(seconds: seconds), () {});
  }

  static void showToast(String msg, {Toast toastLength = Toast.LENGTH_LONG}) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: toastLength,
      timeInSecForIosWeb: 3,
    );
  }

  static void showErrorToast(e) {
    Fluttertoast.showToast(
        msg: e.toString().replaceAll('Exception: ', ''),
        toastLength: Toast.LENGTH_LONG,
        timeInSecForIosWeb: 3);
  }

  static void showLoadingPopup(BuildContext context, {isVisible = true}) {
    if (isVisible)
      showGeneralDialog(
        context: context,
        barrierColor: Colors.black12.withOpacity(0.2),
        // background color
        barrierDismissible: false,
        // should dialog be dismissed when tapped outside
        barrierLabel: "Dialog",
        // label for barrier
        transitionDuration: Duration(milliseconds: 400),
        // how long it takes to popup dialog after button click
        pageBuilder: (_, __, ___) {
          // your widget implementation
          return WillPopScope(
            onWillPop: () async => false,
            child: Center(
              child: Container(
                alignment: Alignment.center,
                height: 80,
                width: 80,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8),
                ),
                child: SpinKitFadingCircle(
                  color: AgrikuColors.AgrikuColor1,
                  size: 50.0,
                ),
              ),
            ),
          );
        },
      );
    else
      showGeneralDialog(
        context: context,
        barrierColor: Colors.black12.withOpacity(0.01),
        // background color
        barrierDismissible: false,
        // should dialog be dismissed when tapped outside
        barrierLabel: "Dialog",
        // label for barrier
        transitionDuration: Duration(milliseconds: 400),
        // how long it takes to popup dialog after button click
        pageBuilder: (_, __, ___) {
          // your widget implementation
          return WillPopScope(
            onWillPop: () async => false,
            child: Container(),
          );
        },
      );
  }

  static void dismissLoadingPopup(BuildContext context) {
    Navigator.pop(context);
  }

}

import 'package:agriku/src/models/dota_hero_list_response_model.dart';
import 'package:agriku/src/resources/api_helper.dart';

class Repository{

  ApiHelper apiHelper = ApiHelper();

  static const String LISTHERO = 'herostats/';

  // Future<List<DotaHeroListResponseModel>> heroList() async {
  //   final response = await apiHelper.get(LISTHERO);
  //   final result = DotaHeroListResponseModel.fromJson(response);
  //   return result;
  // }
  Future<List<DotaHeroListResponseModel>> getListHero() async {
    return await apiHelper.getHeros(LISTHERO);
  }
}
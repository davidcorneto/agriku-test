import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:agriku/src/models/dota_hero_list_response_model.dart';
import 'package:agriku/src/resources/config.dart';
import 'package:agriku/src/utilities/app_exceptions.dart';
import 'package:http/http.dart' as client;

class ApiHelper {
  ApiHelper._instantiate();

  ApiHelper();

  static final ApiHelper instance = ApiHelper._instantiate();

  List<String> baseUrl = getBaseUrl();

  var header = new Map<String, String>();

  static List<String> getBaseUrl() {
    return Configs.baseUrl;
  }

  static Duration _getTimeOutDuration() {
    return Configs.timeOutDuration;
  }

  Future<Map<String, String>> getHeader() async {
    var result = Map<String, String>();
    result["Content-Type"] = "application/json";
    print('header : $result');
    return result;
  }

  Future<dynamic> get(String url,
      {dynamic params, int baseUrlIndex = 0}) async {
    print('Api Get, url ${baseUrl[baseUrlIndex] + url}');
    print(url);
    String _url = baseUrl[baseUrlIndex] + url;

    var responseJson;
    try {
      final response = await client
          .get(Uri.parse(_url), headers: await getHeader())
          .timeout(_getTimeOutDuration());

      print('RESPONSE ${response.body}');
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    } on TimeoutException {
      print(
          'Koneksi Anda lemah, Pastikan internet Anda lancar');
      throw FetchDataException(
          'Koneksi Anda lemah, Pastikan internet Anda lancar');
    }

    print('api get recieved!');
    return responseJson;
  }

  Future<List<DotaHeroListResponseModel>> getHeros(String url) async {
    print('Api Get, url https://api.opendota.com/api/$url}');
    try{
      var response = await client.get(Uri.parse(
          'https://api.opendota.com/api/$url'));
      // var responseJson;
      // responseJson = _returnResponse(response);

      var parsedJson = json.decode(response.body) as List;
      // print('DATA $responseJson');
      print('DATA $parsedJson');
      // return parsedJson;
      List<DotaHeroListResponseModel> result = List<DotaHeroListResponseModel>.from(parsedJson.map((model)=> DotaHeroListResponseModel.fromJson(model)));
      // return responseJson;
      
      return result;
      // return parsedJson.map((e) => DotaHeroListResponseModel.fromJson(e)).toList();
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    } on TimeoutException {
      print(
          'Koneksi Anda lemah, Pastikan internet Anda lancar');
      throw FetchDataException(
          'Koneksi Anda lemah, Pastikan internet Anda lancar');
    }
  }

  Future<dynamic> post(String url,
      {dynamic params, int baseUrlIndex = 0}) async {
    print('Api Post, url ${baseUrl[baseUrlIndex] + url}');
    String bodyString = '';
    if (params != null) {
      bodyString = jsonEncode(params);
    }
    print('Params : $bodyString');

    String _url = baseUrl[baseUrlIndex] + url;

    var responseJson;
    try {
      final response = await client
          .post(Uri.parse(_url), body: bodyString, headers: await getHeader())
          .timeout(_getTimeOutDuration());

      print('RESPONSE ${response.body}');
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No Network');
      throw FetchDataException('No Internet connection');
    } on TimeoutException {
      print(
          'Koneksi Anda lemah, Pastikan internet Anda lancar');
      throw FetchDataException(
          'Koneksi Anda lemah, Pastikan internet Anda lancar');
    }

    return responseJson;
  }

  dynamic _returnResponse(client.Response response) {
    //TODO: Bikin force logout dari utilities force logout
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        if (responseJson["status"].toString() == 'error') {
          if (responseJson["data"] != null) {
            if ((responseJson["message"] ?? '')
                    .toString()
                    .toLowerCase()
                    .contains('invalid username or password')) {
              throw Exception(responseJson["message"] ?? '');
            }
          } else {
            throw Exception('Oops! Something Went Wrong...');
          }
        }
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 413:
        throw UnauthorisedException('Ukuran file terlalu besar');
      case 500:
      case 502:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server.\nStatusCode :${response.statusCode}');
    }
  }

}
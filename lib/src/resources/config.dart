import 'package:agriku/src/constants/agriku_enums.dart';

class Configs{

  //Generate apliaksi untuk API Staging
  static const AppEnvironmentEnum appEnvironment = AppEnvironmentEnum.STAGING;

  //Nyalakan kalau nanti ada API Production - Generate apliaksi untuk API Production
  // static const AppEnvironmentEnum appEnvironment = AppEnvironmentEnum.PRODUCTION;

  //List base url API staging
  static const _baseUrlStaging = [
    'https://api.opendota.com/api/',
  ];

  //List base url API Prod
  static const _baseUrlProduction = [
    'http://restapi.adequateshop.com/api/',
  ];

  //Untuk get baseurl
  static List<String> get baseUrl {
    List<String> result;
    switch (appEnvironment) {
      case AppEnvironmentEnum.STAGING:
        result = _baseUrlStaging;
        break;
      case AppEnvironmentEnum.PRODUCTION:
        result = _baseUrlProduction;
        break;
    }
    return result;
  }

  //Buat time out duration kalau internet lemah
  static Duration get timeOutDuration {
    Duration result;

    switch (appEnvironment) {
      case AppEnvironmentEnum.STAGING:
        result = Duration(seconds: 60);
        break;
      case AppEnvironmentEnum.PRODUCTION:
        result = Duration(minutes: 3);
        break;
    }
    return result;
  }
}
import 'package:agriku/src/blocs/bloc_provider.dart';
import 'package:agriku/src/blocs/home/home_bloc.dart';
import 'package:agriku/src/constants/agriku_colors.dart';
import 'package:agriku/src/utilities/utilities.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  static const String routeName = '/home';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final bloc = BlocProvider.of<HomeBloc>(context);

    return GestureDetector(
      onTap: () {
        Utilities.dismissKeyboard(context);
      },
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.dark,
          backgroundColor: AgrikuColors.AgrikuColor1,
          centerTitle: true,
          title: Image.asset(
            'assets/images/dota_logo_text.png',
            height: 20,
          ),
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: SafeArea(
          child: Column(
            children: [
              TabBar(
                controller: tabController,
                labelStyle: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w800,
                ),
                unselectedLabelStyle: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w800,
                ),
                labelColor: AgrikuColors.AgrikuColor1,
                unselectedLabelColor: AgrikuColors.text300,
                indicatorColor: Colors.transparent,
                onTap: (index) {},
                tabs: [
                  Tab(text: 'All Heros'),
                  Tab(text: 'Disabler'),
                ],
              ),
              Expanded(
                child: TabBarView(
                  controller: tabController,
                  physics: ScrollPhysics(),
                  children: bloc.getTabBarViewChildren(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

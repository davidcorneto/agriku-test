
import 'package:agriku/src/blocs/bloc_provider.dart';
import 'package:agriku/src/blocs/home/home_hero_list_bloc.dart';
import 'package:agriku/src/constants/agriku_colors.dart';
import 'package:agriku/src/models/dota_hero_list_response_model.dart';
import 'package:agriku/src/resources/api_response.dart';
import 'package:agriku/src/widgets/loading_widget.dart';
import 'package:agriku/src/widgets/no_data.dart';
import 'package:agriku/src/widgets/no_internet.dart';
import 'package:flutter/material.dart';

class HomeHeroListScreen extends StatelessWidget {
  const HomeHeroListScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<HomeHeroListBloc>(context);
    final size = MediaQuery.of(context).size;
    return commonBody(size: size, bloc: bloc);

  }

  Widget commonBody({HomeHeroListBloc bloc, Size size}) {
    return StreamBuilder<ApiResponse<List<DotaHeroListResponseModel>>>(
      stream: bloc.listHero,
      builder: (context, snapshot) {
        final data = snapshot?.data;
        print('DATA ${data?.data}');

        switch (data?.status) {
          case Status.LOADING:
            return LoadingWidget();
            break;
          case Status.COMPLETED:
            final heros = data?.data ?? [];
            if (heros.length == 0) return noData(size: size);
            return ListView(
              shrinkWrap: true,
              children: [
                GridView.count(
                  crossAxisCount: 2,
                  shrinkWrap: true,
                  childAspectRatio: (size.width * 0.4) /
                      (size.width * 0.4 + (size.width * 0.05)),
                  physics: ScrollPhysics(),
                  children: List<Widget>.from(heros.map((e) {
                    return itemWidget(
                        size: size,
                        name: e.name,
                        image: e.img,
                        role: e.roles.join(','),
                        onClicked: () {
                          // bloc.itemClicked(e.productId);
                        });
                  })),
                ),
              ],
            );
            break;
          case Status.ERROR:
            // return NoInternet(
            //   onClicked: bloc.getData(),
            // );
            // break;
          default:
            print('Error');
            return Container();
            break;
        }
      },
    );
  }

  Widget itemWidget({
    Size size,
    String name,
    String image,
    String role,
    String primaryAttr,
    Function onClicked,
  }) {
    name = name ?? '';
    image = image ?? '';
    role = role ?? '';
    primaryAttr = primaryAttr ?? '';

    return Column(
      children: [
        GestureDetector(
          onTap: () {
            if (onClicked != null) onClicked();
          },
          child: Hero(
            tag: name,
            child: Container(
              width: size.width * 0.4,
              height: size.width * 0.4,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: image.isNotEmpty ? AgrikuColors.text200 : AgrikuColors.AgrikuColor1,
              ),
              child: image.isNotEmpty
                  ? ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: Image.network(
                  image,
                  fit: BoxFit.cover,
                ),
              )
                  : Center(
                child: Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/images/ic_image.png'),
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        SizedBox(height: 16),
        Flexible(
          child: Container(
            width: size.width * 0.4,
            // height: size.width * 0.15,
            // color: Colors.red,
            child: Text(
              '$name',
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: AgrikuColors.text500,
              ),
            ),
          ),
        ),
        Container(
          width: size.width * 0.4,
          child: Text(
            '$role',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w800,
              color: AgrikuColors.text200,
            ),
          ),
        ),
      ],
    );
  }
}

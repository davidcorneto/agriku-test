import 'package:agriku/src/blocs/bloc_provider.dart';
import 'package:agriku/src/blocs/splash/splash_bloc.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  static const String routeName = '/';

  const SplashScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<SplashBloc>(context);

    return Scaffold(
      body: Container(
        color: Colors.black,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Image.asset(
              'assets/images/logo_dota.jpg',
              width: 100,
              height: 100,
            ),
          ],
        ),
      ),
    );
  }
}

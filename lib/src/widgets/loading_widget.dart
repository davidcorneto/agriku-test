import 'package:agriku/src/constants/agriku_colors.dart';
import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
        backgroundColor: Colors.transparent,
        valueColor: AlwaysStoppedAnimation(AgrikuColors.grey200),
        strokeWidth: 3,
      ),
    );
  }
}
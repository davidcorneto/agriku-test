import 'package:agriku/src/constants/agriku_colors.dart';
import 'package:flutter/material.dart';

class NoInternet extends StatefulWidget {
  final Function onClicked;
  NoInternet({this.onClicked});
  @override
  _NoInternetState createState() => _NoInternetState();
}

class _NoInternetState extends State<NoInternet> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Error',
          ),
          FlatButton(
            onPressed: () {
              if (widget.onClicked != null) widget.onClicked();
            },
            color: AgrikuColors.text400,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Text(
              'Retry',
              style: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: 12,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
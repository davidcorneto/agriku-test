import 'package:agriku/src/constants/agriku_colors.dart';
import 'package:flutter/material.dart';

Widget noData({Size size}) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Image.asset(
        'assets/images/ic_not_found.png',
        height: size.width * 0.20,
        width: size.width * 0.20,
      ),
      Text(
        'Data tidak ditemukan',
        style: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize:12,
          color: AgrikuColors.text300,
        ),
      ),
    ],
  );
}
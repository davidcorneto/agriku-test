import 'package:flutter/material.dart';

class AgrikuColors {
  static Color AgrikuColor1 = Color(0xFF2b2b2b);
  static Color AgrikuColor2 = Color(0xFFFFC90E);
  static Color AgrikuColor3 = Color(0xFF58489D);
  static Color AgrikuColor4 = Color(0xFF41C0F0);
  static Color AgrikuColor5 = Color(0xFF4C4C4C);
  static Color AgrikuColor6 = Color(0xFFA0A0A0);
  static Color AgrikuColor7 = Color(0xFFC8C8C8);
  static Color AgrikuColor8 = Color(0xFFF5F6F8);

  static Color otaquTextFieldColor = Color(0xFFF3E7FF);

  static Color grey = Colors.grey;
  static Color grey50 = Colors.grey[50];
  static Color grey100 = Colors.grey[100];
  static Color grey200 = Colors.grey[200];
  static Color grey250 = Colors.grey[250];
  static Color grey300 = Colors.grey[300];
  static Color grey350 = Colors.grey[350];
  static Color grey400 = Colors.grey[400];
  static Color grey600 = Colors.grey[600];
  static Color greyPlaceHolder = Color(0xFFE8E9E8);

  static Color black = Colors.black;
  static Color black87 = Colors.black87;
  static Color black54 = Colors.black54;
  static Color black45 = Colors.black45;

  static const Color text500 = Color(0xFF323232);
  static const Color text400 = Color(0xFF636363);
  static const Color text300 = Color(0xFFABABAB);
  static const Color text200 = Color(0xFFE2E2E2);
  static const Color text100 = Color(0xFFF7F7F7);

  static Color white = Colors.white;

  static Color defaultCream = Color(0xFFFEE6DA);

  static Color transparent = Colors.transparent;

  static Color colorFromHex(String hexColor) {
    if (hexColor == null) return Colors.grey;

    try {
      hexColor = hexColor.toUpperCase().replaceAll("#", "");
      if (hexColor.length == 6) {
        hexColor = "FF" + hexColor;
      }
      return Color(int.parse(hexColor, radix: 16));
    } catch (e, s) {
      print(s);
      return Colors.grey;
    }
  }
}
